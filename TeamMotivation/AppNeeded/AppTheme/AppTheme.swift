//
//  AppTheme.swift
//   TeamMotivation
//
//  Created by Admin on 7/31/19.
//  Copyright © 2021 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    static let green =  UIColor(hex:0x77BC25)
    static let bluish = UIColor(hex:0x2493a2)
    static let warmGrey = UIColor(hex:0x707070)
    static let lightNavyBlue = UIColor(hex:0x304a78)
    static let black = UIColor(hex:0x131313)
    static let white = UIColor(hex:0xffffff)
    static let camel = UIColor(hex:0xbf9e46)

}

enum ColorTheme: Float {
    
    case green = 1.1
    case bluish = 1.2
    case warmGrey = 1.3
    case lightNavyBlue = 1.4
    case black = 1.5
    case white = 1.6
    case camel = 1.7
        
    var color: UIColor {
        switch self {
        case .green: return UIColor.green
        case .bluish: return UIColor.bluish
        case .warmGrey: return UIColor.warmGrey
        case .lightNavyBlue: return UIColor.lightNavyBlue
        case .black: return UIColor.black
        case .white: return UIColor.white
        case .camel: return UIColor.camel
        }
    }
}

struct Theme {
    var background: UIColor
    var text: UIColor
    var tint: UIColor
    var border: UIColor
    
    init(background: UIColor = UIColor.clear, text: UIColor = UIColor.bluish, tint: UIColor = UIColor.blue, border: UIColor = UIColor.clear) {
        self.background = background
        self.text = text
        self.tint = tint
        self.border = border
    }
}
