//
//  AppLabel.swift
//   TeamMotivation
//
//  Created by Admin on 8/4/19.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit

@IBDesignable
class AppLabel: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBInspectable var textThemeColor: Float = -1 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var backgroundThemeColor: Float = -1 {
        didSet {
            setup()
        }
    }
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        setup()
    }
    
    convenience init() {
        self.init(frame:CGRect.zero)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    func setup() {
        adjustsFontForContentSizeCategory = true
        if self.textThemeColor != -1 {
            self.textColor = ColorTheme.init(rawValue: self.textThemeColor)?.color ?? UIColor.black
        }
        if self.backgroundThemeColor != -1 {
            self.backgroundColor = ColorTheme.init(rawValue: self.backgroundThemeColor)?.color ?? UIColor.white
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

}
