//
//  AlertController.swift
//  TeamMotivation
//
//  Created by Admin on 6/29/21.
//

import UIKit

class AlertController: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel! {
        didSet {
            self.titleLbl?.text = "Alert"
        }
    }
    @IBOutlet weak var messageLbl: UILabel!
    
    @IBOutlet weak var defaultBtn: UIButton!
    @IBOutlet weak var destructiveBtn: UIButton!
    
    var alertType: AlertType = .Info
    
    var alertInfo: (title: String?, msg: String?, actionPhrase1: String?, actionPhrase2: String?, completion: ((UIAlertAction.Style) -> (Void))?)?
    
    static func initWith(title: String?, message: String?, actionPhrase1: String? = nil, actionPhrase2: String? = nil, completion: ((UIAlertAction.Style) -> (Void))? = nil) -> AlertController {
        let alertVC = AlertController.initFromStoryBoard("Utils")
        alertVC.alertType = actionPhrase2 != nil && actionPhrase1 != nil ? .Dialog : .Info
        alertVC.alertInfo = (title: title, msg: message, actionPhrase1: actionPhrase1, actionPhrase2: actionPhrase2, completion: completion)
        return alertVC
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        defaultBtn?.setTitle(alertInfo?.actionPhrase1, for: .normal)
        destructiveBtn?.setTitle(alertInfo?.actionPhrase2, for: .normal)
        messageLbl?.text = alertInfo?.msg
        titleLbl?.text = alertInfo?.title
                
        if alertType == .Info {
            destructiveBtn?.isHidden = true
        }
    }
    
//    @IBAction func defaultAction() {
//        alertInfo?.completion?(.default)
//        self.backBtnPressed()
//    }
//
//    @IBAction func destructiveAction() {
//        alertInfo?.completion?(.destructive)
//        self.backBtnPressed()
//    }

}

enum AlertType: String {
    case Info, Dialog
}
extension NSObject {
    var className: String {
        return String(describing: type(of: self))
    }
}
extension UIViewController {
    @objc func resetToInit() {
        
    }
}

extension UIViewController {
    func push(_ viewController: UIViewController, animated: Bool = true) {
        let navVC = (self as? UINavigationController) ?? self.navigationController
        if let vc = navVC?.children.filter({$0.className == viewController.className }).first {
            vc.resetToInit()
            navVC?.popToViewController(vc, animated: true)
            return
        }
        navVC?.pushViewController(viewController, animated: animated)
    }
    
    func pop(_ animated: Bool = true) {
        self.navigationController?.popViewController(animated: animated)
    }
    
    func popTo(_ viewController: UIViewController, animated: Bool = true) {
        self.navigationController?.popToViewController(viewController, animated: animated)
    }
}

extension UIViewController
{
    class func initFromStoryBoard(_ name: String = "Main") -> Self
    {
        return instantiateFromStoryboardHelper(name)
    }

    fileprivate class func instantiateFromStoryboardHelper<T>(_ name: String) -> T
    {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! T
        return controller
    }
}
