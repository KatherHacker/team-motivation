//
//  MyCell.swift
//  DropDown
//
//  Created by Kevin Hirsch on 17/08/16.
//  Copyright © 2016 Kevin Hirsch. All rights reserved.
//

import UIKit
import DropDown

class MCDropDownCell: DropDownCell {
    @IBOutlet weak var expandArrow: UIImageView!
    @IBOutlet weak var leftRoundedView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        expandArrow?.isHidden = true
        leftRoundedView?.isHidden = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.leftRoundedView?.cornerRadius = self.leftRoundedView.bounds.width/2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        expandArrow?.isHidden = true
        leftRoundedView?.isHidden = true
    }
}
