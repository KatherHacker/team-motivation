//
//  CoreDataService.swift
//   L2L
//
//  Created by Admin on 27/04/21.
//

import UIKit

/// NSTimeInterval Constant for the data service request
let timeOutInterval: TimeInterval = 20

/**
 This Method is used to check the error objects based upon their error codes
 
 - parameter lhs: error to the left side
 - parameter rhs: error to the right side
 
 - returns: returns true if lhs equal to rhs, otherwise returns false
 */
public func == (lhs: CoreError, rhs: CoreError) -> Bool {
    return lhs.code == rhs.code
}

/// Class for ErrorInformation Model
open class ErrorInformation {
    /// Public constant that denotes the error (or) error information code
    public let code: Int
    /// Public variable that describes what kind of error (or) error information
    open var description: String
    
    /**
     Initializer method for ErrorInformation() Class
     
     - parameter code:        error code
     - parameter description: error description
     
     - returns: returns the instance of the ErrorInformation() Class
     */
    public init(code: Int, description: String) {
        self.code = code
        self.description = description
    }
}

/// Class for Error Model that inherits ErrorInformation, ErrorType and Equatabe Classes
open class CoreError: ErrorInformation, Error, Equatable {
    /// Public constant that denotes the inner error of the Error object (ie. Error with in the error)
    public let innerError: CoreError?
    /// Public variable that is used to store list of error infomation of the roor error
    open var informations: [ErrorInformation]?
    
    /**
     Initializer method for the Error() Class
     
     - parameter code:         error code
     - parameter description:  error description
     - parameter innerError:   inner of the error object
     - parameter informations: array of error informations
     
     - returns: returns the instance of the Error() class
     */
    public init(code: Int, description: String, innerError: CoreError?, informations: [ErrorInformation]?) {
        self.innerError = innerError
        self.informations = informations
        super.init(code: code, description: description)
    }
}

/**
 Enum to classify data service response according to their response codes
 
 - Ok:                  Denotes response with the code Ok
 - Created:             Denotes response with the code Created
 - Forbidden:           Denotes response with the code Forbidden error
 - UnAuthorized:        Denotes response with the code UnAuthorized error
 - NotFound:            Denotes response with the code NotFound error
 - UnProcessableEntity: Denotes response with the code UnProcessableEntity error
 - Internal:            Denotes response with the code Internal Error
 - NoContent:           Denotes response with the code NoContent Error
 */
public enum HTTPStatusCode: Int {
    case ok = 200, created = 201, accepted = 202, forbidden = 403, unAuthorized = 401, notFound = 404, unProcessableEntity = 422, `internal` = 500, noContent = 204, badRequest = 400
}

open class TimeOutError: CoreError {
    init(innerError: CoreError?) {
        super.init(code: NSURLErrorTimedOut, description: HTTPURLResponse.localizedString(forStatusCode: 408), innerError: innerError, informations: nil)
    }
}

open class NetWorkNotReachError: CoreError {
    init(innerError: CoreError?) {
        super.init(code: 001, description: HTTPURLResponse.localizedString(forStatusCode: 408), innerError: innerError, informations: nil)
    }
}

/// Class for UnAuthorisedError Model that inherits Error Class
open class UnAuthorisedError: CoreError {
    /**
     Initializer method for the UnAuthorisedError() Class
     
     - parameter innerError: inner of the error object
     
     - returns: returns the instance of the UnAuthorisedError() class
     */
    init(innerError: CoreError?) {
        super.init(code: HTTPStatusCode.unAuthorized.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.unAuthorized.rawValue), innerError: innerError, informations: nil)
    }
}

/// Class for UnAuthorisedError Model that inherits Error Class
open class UserTokenNotProvideError: CoreError {
    /**
     Initializer method for the UnAuthorisedError() Class
     
     - parameter innerError: inner of the error object
     
     - returns: returns the instance of the UnAuthorisedError() class
     */
    init(innerError: CoreError?) {
        super.init(code: HTTPStatusCode.unAuthorized.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.unAuthorized.rawValue), innerError: innerError, informations: nil)
    }
}

/// Class for ForbiddenError Model that inherits Error Class
open class ForbiddenError: CoreError {
    
    /**
     Initializer method for the ForbiddenError() Class
     
     - parameter innerError: inner of the error object
     
     - returns: returns the instance of the ForbiddenError() class
     */
    init(innerError: CoreError?) {
        super.init(code: HTTPStatusCode.forbidden.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.forbidden.rawValue), innerError: innerError, informations: nil)
    }
}

/// Class for InternalError Model that inherits Error Class
open class InternalError: CoreError {
    /**
     Initializer method for the InternalError() Class
     
     - parameter innerError: inner of the error object
     
     - returns: returns the instance of the InternalError() class
     */
    init(innerError: CoreError?) {
        super.init(code: HTTPStatusCode.internal.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.internal.rawValue), innerError: innerError, informations: nil)
    }
}

/// Class for UnProcessableEntityError Model that inherits Error Class
open class UnProcessableEntityError: CoreError {
    /**
     Initializer method for the UnProcessableEntityError() Class
     
     - parameter innerError: inner of the error object
     
     - returns: returns the instance of the UnProcessableEntityError() class
     */
    init(innerError: CoreError?, informations: [ErrorInformation]?) {
        super.init(code: HTTPStatusCode.unProcessableEntity.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.unProcessableEntity.rawValue), innerError: innerError, informations: informations)
    }
}

/// Class for NotFoundError Model that inherits Error Class
open class NotFoundError: CoreError {
    
    /**
     Initializer method for the NotFoundError() Class
     
     - parameter innerError: inner of the error object
     
     - returns: returns the instance of the NotFoundError() class
     */
    init(innerError: CoreError?, informations: [ErrorInformation]?) {
        super.init(code: HTTPStatusCode.notFound.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.notFound.rawValue), innerError: innerError, informations: informations)
    }
}

// Class for  BadRequestError Model that inherits Error Class
open class BadRequestError: CoreError {
    
    /**
     Initializer method for the BadRequestError() Class
     
     - parameter innerError: inner of the error object
     
     - returns: returns the instance of the NotFoundError() class
     */
    init(innerError: CoreError?, informations: [ErrorInformation]?) {
        super.init(code: HTTPStatusCode.badRequest.rawValue, description: HTTPURLResponse.localizedString(forStatusCode: HTTPStatusCode.badRequest.rawValue), innerError: innerError, informations: informations)
    }
}

/**
 Enum to classify the type of a request method
 
 - Post:   request method of type POST
 - Get:    request method of type GET
 - Put:    request method of type PUT
 - Delete: request method of type DELETE
 */
public enum HTTPVerb: String {
    case Post = "POST", Get = "GET", Put = "PUT", Delete = "DELETE", Patch = "PATCH"
}

/**
 Enum to classify the type of a request method
 
 - Post:   request method of type POST
 - Get:    request method of type GET
 - Put:    request method of type PUT
 - Delete: request method of type DELETE
 */
public enum AuthroizationType {
    case none, application, user
}

/**
 *  @author , 15-10-19 19:10:16
 *
 *  This Contains the Request EndpointInfo
 *
 *  @since version 1.0
 */
public struct CoreDataServiceEndPointInfo {
    /// variable to mention the scheme of the service endpoint
    var scheme: String
    /// variable to mention the host of the service endpoint
    var host: String
    /// variable to mention the port of the service endpoint
    var port: NSNumber
    /// variable to mention the path of the service endpoint
    var path: String
    /// enum variable to mention the type of the service endpoint
    var verb: HTTPVerb
    /// enum variable to mention the type of the Authroization Type for service
    public var authroizationType: AuthroizationType
    
    /**
     Initializer method for the CoreDataServiceEndPointInfo() struct
     
     - parameter scheme: scheme of the service endpoint
     - parameter host:   host of the service endpoint
     - parameter port:   port of the service endpoint
     - parameter path:   path of the service endpoint
     - parameter verb:   type of the service endpoint
     r
     - returns: returns the instance of the CoreDataServiceEndPointInfo() struct
     */
    public init(scheme: String, host: String, port: NSNumber, path: String, verb: HTTPVerb, authroizationType: AuthroizationType) {
        
        self.scheme = scheme
        self.host = host
        self.port = port
        self.path = path
        self.verb = verb
        self.authroizationType = authroizationType
        
    }
}

/**
 *  @author , 15-10-19 20:10:13
 *
 *  Protocol for Servive EndPoint to various components
 *
 *  @since version 1.0
 */
public protocol AddressableEndPoint {
    
    var endPointInfo: CoreDataServiceEndPointInfo { get }
    
}

enum NetWorkReachability: Int {
    case notReachable1 = 0, reachableViaWiFi1 = 1, reachableViaWWAN1 = 2
}

public enum RequestContentType: String {
    case json = "application/json", formurlencoded = "application/x-www-form-urlencoded", multipartformdata = "multipart/form-data"
}

/// This Class contains Webservice Operations
open class CoreDataService: NSObject {
    public enum ProjectEnvironment {
        case develop, test, stage, production
    }
    
    open var currentEnvironment: ProjectEnvironment!
    open var userAutheticateToken: AnyObject?
    open var applicationAutheticateToken: AnyObject?
    var currentReachability: NetWorkReachability
    open var requestContentType: RequestContentType { return .json }
    
    public override init() {
        let cuStatus = UserDefaults.standard.object(forKey: "CurrentNetworkStatus")
        if let objec = cuStatus as? String {
            switch objec {
            case "0":
                currentReachability = .notReachable1
            case "1":
                currentReachability = .reachableViaWiFi1
            case "2":
                currentReachability = .reachableViaWWAN1
            default:
                currentReachability = .notReachable1
            }
        } else {
            currentReachability = .reachableViaWiFi1
        }
        
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(CoreDataService.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: "networkStatusChanged"), object: nil)
    }
    
    @objc func networkStatusChanged(_ note: Notification) {
        if let objec = note.object as? String {
            switch objec {
            case "0":
                currentReachability = .notReachable1
            case "1":
                currentReachability = .reachableViaWiFi1
            case "2":
                currentReachability = .reachableViaWWAN1
            default:
                currentReachability = .notReachable1
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "networkStatusChanged"), object: nil)
    }
    
    /**
     This Method is used to call the Webservice and Returns the Response
     
     - parameter endPoint:   This Contains URL Endpoints of the Webservice
     - parameter parameters: This Contains parameters in the url
     - parameter body:       This Contains the Request Body
     - parameter headers:    This Contians the URL Headers
     - parameter pathSuffix: This Contains the URL Path Suffix
     - parameter completion: This returns the Response Result
     */
    
    open func requestEndPoint(_ endPoint: AddressableEndPoint, withParameters parameters: AnyObject?, withBody body: AnyObject?, withHeaders headers: AnyObject?, andPathSuffix pathSuffix: AnyObject?, onCompletion completion: @escaping (_ inner: () throws -> Data?, _ header: AnyObject?) -> Void) {
        
        //        self.applicationAutheticateToken = CacheManager.sharedInstance.getObjectInApplicationForKey(.AuthToken)
        //        self.userAutheticateToken = CacheManager.sharedInstance.getObjectForKey(.AuthToken)
        
        if self.currentReachability == .notReachable1 {
            completion({ () -> Data? in
                throw NetWorkNotReachError(innerError: nil)
            }, nil)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "NetWorkFaildToReach"), object: "\(1)")
            return
        }
        
        var endPointInfo = endPoint.endPointInfo
        print("BODY = \(body ?? "" as AnyObject)")
        if let _ = pathSuffix {
            endPointInfo.path = endPointInfo.path + String(describing: pathSuffix!)
        }
        
        switch endPointInfo.authroizationType {
        case .none:
            break
            
        case .application:
            if self.userAutheticateToken != nil {
                
            } else if (self.applicationAutheticateToken != nil) {
                
            } else {
                completion({ () -> Data? in
                    throw CoreError(code: 999, description: "Application_Token_Not_Provided", innerError: nil, informations: nil)
                }, nil)
                return
            }
            break
            
        case .user:
            if self.userAutheticateToken != nil {
                
            } else {
                completion({ () -> Data? in
                    throw UserTokenNotProvideError(innerError: CoreError(code: 999, description: "User_Token_Not_Provided", innerError: nil, informations: nil))
                }, nil)
                return
            }
            break
        }
        
        let languageString: String
        var language = "en"
        if let _lang = UserDefaults.standard.object(forKey: "CurrentApplicationLanguage") as? String {
            language = _lang
        }
        let localeArr = language.components(separatedBy: "-")
        languageString = localeArr[0]
        print(languageString)
        var urlComponents = URLComponents()
        urlComponents.scheme = endPointInfo.scheme
        if endPointInfo.port != 0 {
            urlComponents.port = endPointInfo.port.intValue
        }
        urlComponents.host = endPointInfo.host
        urlComponents.path = endPointInfo.path
        
        var queryString: String?
        // TODO: We cannot able to sent parameter for except get or delete service calls.
        if (endPointInfo.verb == .Get || endPointInfo.verb == .Delete) {
            //            var queryBodyStrings = [String]()
            //            if let queryBody = parameters as? [String: AnyObject], queryBody.values.count > 0 {
            //                for (key, value) in queryBody {
            //                    queryBodyStrings.append("\(key)=\(value)")
            //                }
            //            }
            //            if (endPointInfo.verb == .Get) {
            //                queryBodyStrings.append(String(format: "locale=%@", languageString))
            //            }
            queryString = self.formQueryBodyString(parameters)
        }
        if let _queryString = queryString {
            urlComponents.query = _queryString
        }
        
        print("urlComponents \(urlComponents)")
        if let url = urlComponents.url {
            
            var urlRequest = NSMutableURLRequest(url: url, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: timeOutInterval)
            
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            let boundary = generateBoundaryString()
            if self.requestContentType == .multipartformdata {
                urlRequest.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
            } else {
                urlRequest.setValue(self.requestContentType.rawValue, forHTTPHeaderField: "Content-Type")
            }
            
            if let urlHeaders = headers as? [String: String], urlHeaders.values.count > 0 {
                urlRequest = appendHeaders(urlHeaders as AnyObject?, withURLRequest: urlRequest)
            }
            
            if endPointInfo.authroizationType != .none {
                if self.userAutheticateToken != nil {
                    var urlString = ""
                    if let tempString = self.userAutheticateToken as? String {
                        urlString = "Bearer " + tempString
                    }
                    urlRequest = appendHeaders(["Authorization": urlString] as AnyObject?, withURLRequest: urlRequest)
                } else if (endPointInfo.authroizationType != .user && self.applicationAutheticateToken != nil) {
                    var urlString = ""
                    if let tempString = self.applicationAutheticateToken as? String {
                        urlString = tempString//"Bearer " + tempString
                    }
                    urlRequest = appendHeaders(["Authorization": urlString] as AnyObject?, withURLRequest: urlRequest)
                }
            }
            
            urlRequest.httpMethod = endPointInfo.verb.rawValue
            print(urlRequest.httpMethod)
            switch endPointInfo.verb {
            case let dataTask where dataTask == HTTPVerb.Post || dataTask == HTTPVerb.Patch || dataTask == HTTPVerb.Put || dataTask == HTTPVerb.Delete:
                if self.requestContentType == .json {
                    if let requestBody = body {
                        do {
                            let data = try JSONSerialization.data(withJSONObject: requestBody, options: JSONSerialization.WritingOptions.prettyPrinted)
                            urlRequest.setValue("\(data.count)", forHTTPHeaderField: "Content-Length")
                            if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                                print(string)
                            }
                            urlRequest.httpBody = data
                        }
                        catch {
                            print("Error occured")
                        }
                    }
                } else if self.requestContentType == .multipartformdata {
                    if let requestBody = body {
                        urlRequest.httpBody = self.createDataBody(withParameters: (requestBody as? [String: Any]), media: [], boundary: boundary)
                    }
                } else {
                    if let requestBody = self.formQueryBodyString(body) {
                        let data = requestBody.data(using: String.Encoding.ascii, allowLossyConversion: false)
                        urlRequest.setValue("\(data?.count ?? 0)", forHTTPHeaderField: "Content-Length")
                        if let string = NSString(data: data ?? Data(), encoding: String.Encoding.utf8.rawValue) {
                            print(string)
                        }
                        urlRequest.httpBody = data
                    }
                }
                break
            case let dataTask where dataTask == HTTPVerb.Get:
                break
            default:
                break
            }
            
            print(urlRequest)
            let isNeedToTrust: Bool = self.currentEnvironment == .production ? false : true
            
            let urlSession = isNeedToTrust ? Foundation.URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main) : Foundation.URLSession.shared
            
            urlSession.dataTask(with: urlRequest as URLRequest, completionHandler: { (data, response, error) -> Void in
                //                OperationQueue.main.addOperation({ () -> Void in
                
                if let requestError = error {
                    
                    if (requestError as NSError).code ==  NSURLErrorTimedOut {
                        completion({ () -> Data? in
                            throw TimeOutError(innerError: nil)
                        }, nil)
                        return
                    }
                    
                    print(requestError)
                    if (requestError as NSError).code == -1009 {
                        DispatchQueue.main.async {
                            completion({ () -> Data? in
                                throw CoreError(code: 001, description: "network is not reach", innerError: nil, informations: nil)
                            }, nil)
                        }
                    } else {
                        DispatchQueue.main.async {
                            completion({ () -> Data? in
                                throw CoreError(code: (requestError as NSError).code, description: (requestError as NSError).description, innerError: nil, informations: nil)
                            }, nil) }
                    }
                }
                
                guard let httpUrlResponse = response as? HTTPURLResponse else {
                    return
                }
                
                //                    CacheManager.sharedInstance.setObject(Date() as AnyObject, key: .LastApiCallAt)
                //                    CacheManager.sharedInstance.saveCache()
                var dataObject: Data?
                
                if let responseData = data, responseData.count > 0 {
                    do {
                        if let codedString = NSString(data: responseData, encoding: String.Encoding.utf8.rawValue) {
                            print(codedString)
                        }
                        dataObject = responseData
                    }
                }
                
                print("Code ================== \(httpUrlResponse.statusCode)")
                switch httpUrlResponse.statusCode {
                    
                case let successCode where Set([HTTPStatusCode.ok.rawValue, HTTPStatusCode.created.rawValue, HTTPStatusCode.accepted.rawValue, HTTPStatusCode.noContent.rawValue]).contains(successCode):
                    print(httpUrlResponse.allHeaderFields)
                    DispatchQueue.main.async {
                        completion({ return dataObject }, httpUrlResponse.allHeaderFields as AnyObject?)
                    }
                    break
                case let httpUrlResponseErrorCode:
                    let innerError: CoreError? = nil
                    let informations: [ErrorInformation]? = []
                    
                    switch (httpUrlResponseErrorCode) {
                    case HTTPStatusCode.unAuthorized.rawValue:
                        DispatchQueue.main.async {
                            
                            completion({ () -> Data? in
                                throw UnAuthorisedError(innerError: innerError)
                            }, nil)
                        }
                    case HTTPStatusCode.forbidden.rawValue:
                        DispatchQueue.main.async {
                            
                            completion({ () -> Data? in
                                throw ForbiddenError(innerError: innerError)
                            }, nil)
                        }
                    case HTTPStatusCode.internal.rawValue:
                        DispatchQueue.main.async {
                            
                            completion({ () -> Data? in
                                throw InternalError(innerError: innerError)
                            }, nil)
                        }
                    case HTTPStatusCode.unProcessableEntity.rawValue:
                        DispatchQueue.main.async {
                            
                            completion({ () -> Data? in
                                throw UnProcessableEntityError(innerError: innerError, informations: informations)
                            }, nil)
                        }
                    case HTTPStatusCode.notFound.rawValue:
                        DispatchQueue.main.async {
                            completion({ () -> Data? in
                                throw NotFoundError(innerError: innerError, informations: informations)
                            }, nil)
                        }
                    case HTTPStatusCode.badRequest.rawValue:
                        let commonRes = dataObject?.getDecodedObject(from: CommonRes.self)
//                        (UIApplication.shared.delegate as? AppDelegate)?.keyWindow?.showToast(commonRes?.detail)
                        DispatchQueue.main.async {
                            completion({ () -> Data? in
                                throw BadRequestError(innerError: innerError, informations: informations)
                            }, nil)
                        }
                    default:
                        DispatchQueue.main.async {
                            completion({ () -> Data? in
                                throw CoreError(code: httpUrlResponseErrorCode, description: "Un Handled Error", innerError: innerError, informations: nil)
                            }, nil)
                        }
                        
                    }
                }
                //                })
            }).resume()
        }
    }
    
    /*
     This Method is Used to Build the query string
     
     - parameter parameters: The param that need to be change as query string
     
     - returns: It returns the query String
     */
    
    func queryStringFromParameters(parameters:AnyObject?)->String{
        var paramString:String = ""
        if let parametersDict = parameters as? [String :AnyObject] {
            for (key,value) in parametersDict {
                paramString = paramString+key+"="+(String(describing: value))+"&"
            }
            paramString = paramString.trimmingCharacters(in: .whitespaces)
            let index = paramString.index(paramString.endIndex, offsetBy: -1)
            paramString = paramString[..<index].lowercased()
        }
        return paramString
    }
    
    /**
     This Method is Used to Append the Headers in URL
     
     - parameter headers:    This is the Header in the URL
     - parameter urlRequest: This is the Mutable URl Request
     
     - returns: The Mutable  URL Request
     */
    
    func appendHeaders(_ headers: AnyObject?, withURLRequest urlRequest: NSMutableURLRequest) -> NSMutableURLRequest {
        if let urlHeaders = headers as? [String: String] {
            for (key, value) in urlHeaders {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }
        #if DEBUG
        print("URL Request Headers = \(String(describing: urlRequest.allHTTPHeaderFields))")
        #endif
        return urlRequest
    }
    
    func formQueryBodyString(_ parameters: AnyObject?) -> String? {
        var queryBodyStrings = [String]()
        if let queryBody = parameters as? [String: AnyObject], queryBody.values.count > 0 {
            for (key, value) in queryBody {
                queryBodyStrings.append("\(key)=\(value)")
            }
        }
        let queryString = (queryBodyStrings as NSArray).componentsJoined(by: "&")
        return queryString
    }
    
    open func uploadImageOrFile(_ image: UIImage? = nil, fileData: Data? = nil, filePathKey: String, endPoint: AddressableEndPoint, withBodyParameters parameters: AnyObject? = nil, withHeaders headers: AnyObject? = nil, andPathSuffix pathSuffix: AnyObject? = nil, mimeType: MimeType = .jpeg, onCompletion completion: @escaping (_ status: () throws -> Data?) -> Void) {
        
        if self.currentReachability == .notReachable1 {
            completion({ throw CoreError(code: 001, description: "network is not reach", innerError: nil, informations: nil)})
            NotificationCenter.default.post(name: Notification.Name(rawValue: "NetWorkFaildToReach"), object: "\(1)")
            return
        }
        
        var imageData = fileData != nil ? fileData : image?.jpegData(compressionQuality: 1)
        
        if(imageData==nil) { imageData = Data() }
        
        var endPointInfo = endPoint.endPointInfo
        if let _ = pathSuffix {
            endPointInfo.path = endPointInfo.path + String(describing: pathSuffix!)
        }
        
        var urlComponents = URLComponents()
        urlComponents.scheme = endPointInfo.scheme
        if endPointInfo.port != 0 {
            urlComponents.port = endPointInfo.port.intValue
        }
        urlComponents.host = endPointInfo.host
        urlComponents.path = endPointInfo.path
        
        
        print("urlComponents \(urlComponents)")
        
        if let url = urlComponents.url {
            var request = NSMutableURLRequest(url: url)
            request.httpMethod = endPointInfo.verb.rawValue
            
            let boundary = generateBoundaryString()
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            switch endPointInfo.authroizationType {
            case .application:
                if (self.applicationAutheticateToken != nil) {
                    
                } else {
                    completion({ throw CoreError(code: 999, description: "Application_Token_Not_Provided", innerError: nil, informations: nil)})
                    return
                }
                break
                
            case .user, .none:
                if self.userAutheticateToken != nil {
                    var urlString = ""
                    if let tempString = self.userAutheticateToken as? String {
                        urlString = "Bearer " + tempString
                    }
                    request = self.appendHeaders(["Authorization": urlString] as AnyObject?, withURLRequest: request)
                } else {
                    completion({ throw UserTokenNotProvideError(innerError: CoreError(code: 999, description: "User_Token_Not_Provided", innerError: nil, informations: nil))})
                    return
                }
                break
            }
            
            var fileTypes: [String: String] = [String: String]()
            fileTypes[MimeType.jpeg.rawValue] = "jpg"
            fileTypes[MimeType.png.rawValue] = "png"
            fileTypes[MimeType.gif.rawValue] = "gif"
            fileTypes[MimeType.pdf.rawValue] = "pdf"
            
            guard let mediaImage = MediaData(image, data: fileData, key: filePathKey, imageName: "\(RAND_MAX)."+(fileTypes[mimeType.rawValue] ?? "jpg"), mimeType:mimeType) else { return }
            request.httpBody = self.createDataBody(withParameters: (parameters as? [String: String]), media: [mediaImage], boundary: boundary)
            URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                
                OperationQueue.main.addOperation({ () -> Void in
                    
                    if let requestError = error {
                        print(requestError)
                        if (requestError as NSError).code == -1009 {
                            completion({ () -> Data? in
                                throw CoreError(code: 001, description: "network is not reach", innerError: nil, informations: nil)
                            })
                        } else {
                            completion({ () -> Data? in
                                throw CoreError(code: (requestError as NSError).code, description: (requestError as NSError).description, innerError: nil, informations: nil)
                            }) }
                    }
                    
                    guard let httpUrlResponse = response as? HTTPURLResponse else {
                        return
                    }
                    
                    var dataObject: Data?
                    
                    if let responseData = data, responseData.count > 0 {
                        if let codedString = NSString(data: responseData, encoding: String.Encoding.utf8.rawValue) {
                            print(codedString)
                        }
                        dataObject = responseData
                    }
                    
                    print("Code ================== \(httpUrlResponse.statusCode)")
                    switch httpUrlResponse.statusCode {
                        
                    case let successCode where Set([HTTPStatusCode.ok.rawValue, HTTPStatusCode.created.rawValue, HTTPStatusCode.accepted.rawValue, HTTPStatusCode.noContent.rawValue]).contains(successCode):
                        print(httpUrlResponse.allHeaderFields)
                        completion({ return dataObject })
                        break
                    case let httpUrlResponseErrorCode:
                        var innerError: CoreError?
                        var informations: [ErrorInformation]?
                    
                        switch (httpUrlResponseErrorCode) {
                        case HTTPStatusCode.unAuthorized.rawValue:
                            completion({ () -> Data? in
                                throw UnAuthorisedError(innerError: innerError)
                            })
                        case HTTPStatusCode.forbidden.rawValue:
                            completion({ () -> Data? in
                                throw ForbiddenError(innerError: innerError)
                            })
                        case HTTPStatusCode.internal.rawValue:
                            completion({ () -> Data? in
                                throw InternalError(innerError: innerError)
                            })
                        case HTTPStatusCode.unProcessableEntity.rawValue:
                            completion({ () -> Data? in
                                throw UnProcessableEntityError(innerError: innerError, informations: informations)
                            })
                        case HTTPStatusCode.notFound.rawValue:
                            completion({ () -> Data? in
                                throw NotFoundError(innerError: innerError, informations: informations)
                            })
                        default:
                            completion({ () -> Data? in
                                throw CoreError(code: httpUrlResponseErrorCode, description: "Un Handled Error", innerError: innerError, informations: nil)
                            })
                        }
                    }
                })
                }.resume()
        }
    }
    
    open func uploadImages(_ info: [String:UIImage], endPoint: AddressableEndPoint, withBodyParameters parameters: AnyObject? = nil, withHeaders headers: AnyObject? = nil, andPathSuffix pathSuffix: AnyObject? = nil, mimeType: MimeType = .jpeg, onCompletion completion: @escaping (_ status: () throws -> Data?) -> Void) {
        
        if self.currentReachability == .notReachable1 {
            completion({ throw CoreError(code: 001, description: "network is not reach", innerError: nil, informations: nil)})
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: "NetWorkFaildToReach"), object: "\(1)")
            return
        }
        
        var endPointInfo = endPoint.endPointInfo
        if let _ = pathSuffix {
            endPointInfo.path = endPointInfo.path + String(describing: pathSuffix!)
        }
        
        var urlComponents = URLComponents()
        urlComponents.scheme = endPointInfo.scheme
        if endPointInfo.port != 0 {
            urlComponents.port = endPointInfo.port.intValue
        }
        urlComponents.host = endPointInfo.host
        urlComponents.path = endPointInfo.path
        
        
        print("urlComponents \(urlComponents)")
        
        if let url = urlComponents.url {
            var request = NSMutableURLRequest(url: url)
            request.httpMethod = endPointInfo.verb.rawValue
            
            let boundary = generateBoundaryString()
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
            switch endPointInfo.authroizationType {
            case .application:
                if (self.applicationAutheticateToken != nil) {
                    
                } else {
                    completion({ throw CoreError(code: 999, description: "Application_Token_Not_Provided", innerError: nil, informations: nil)})
                    return
                }
                break
                
            case .user, .none:
                if self.userAutheticateToken != nil {
                    var urlString = ""
                    if let tempString = self.userAutheticateToken as? String {
                        urlString = "Bearer " + tempString
                    }
                    request = self.appendHeaders(["Authorization": urlString] as AnyObject?, withURLRequest: request)
                } else {
                    completion({ throw UserTokenNotProvideError(innerError: CoreError(code: 999, description: "User_Token_Not_Provided", innerError: nil, informations: nil))})
                    return
                }
                break
            }
            
            var fileTypes: [String: String] = [String: String]()
            fileTypes[MimeType.jpeg.rawValue] = "jpg"
            fileTypes[MimeType.png.rawValue] = "png"
            fileTypes[MimeType.gif.rawValue] = "gif"
            
            var mediaImages = [MediaData]()
            for (key,image) in info {
                if let mediaImage = MediaData(image, key: key, imageName: "\(RAND_MAX)."+(fileTypes[mimeType.rawValue] ?? "jpg"), mimeType:mimeType) {
                    mediaImages.append(mediaImage)
                }
            }
            request.httpBody = self.createDataBody(withParameters: (parameters as? [String: String]), media: mediaImages, boundary: boundary)
            URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                
                OperationQueue.main.addOperation({ () -> Void in
                    
                    if let requestError = error {
                        print(requestError)
                        if (requestError as NSError).code == -1009 {
                            completion({ () -> Data? in
                                throw CoreError(code: 001, description: "network is not reach", innerError: nil, informations: nil)
                            })
                        } else {
                            completion({ () -> Data? in
                                throw CoreError(code: (requestError as NSError).code, description: (requestError as NSError).description, innerError: nil, informations: nil)
                            }) }
                    }
                    
                    guard let httpUrlResponse = response as? HTTPURLResponse else {
                        return
                    }
                    
                    var dataObject: Data?
                    
                    if let responseData = data, responseData.count > 0 {
                        if let codedString = NSString(data: responseData, encoding: String.Encoding.utf8.rawValue) {
                            print(codedString)
                        }
                        dataObject = responseData
                    }
                    
                    print("Code ================== \(httpUrlResponse.statusCode)")
                    switch httpUrlResponse.statusCode {
                        
                    case let successCode where Set([HTTPStatusCode.ok.rawValue, HTTPStatusCode.created.rawValue, HTTPStatusCode.accepted.rawValue, HTTPStatusCode.noContent.rawValue]).contains(successCode):
                        print(httpUrlResponse.allHeaderFields)
                        completion({ return dataObject })
                        break
                    case let httpUrlResponseErrorCode:
                        var innerError: CoreError?
                        var informations: [ErrorInformation]?
                        if let errors = dataObject as? [[String: AnyObject]] {
                            if errors.count > 1 || httpUrlResponseErrorCode == HTTPStatusCode.unProcessableEntity.rawValue {
                                var errorInformations = [ErrorInformation]()
                                for error in errors {
                                    if let errorDescription = error["message"] as? String, let errorCode = error["code"] as? String {
                                        errorInformations.append(ErrorInformation(code: Int(errorCode)!, description: errorDescription))
                                    }
                                }
                                if (errorInformations.count > 0) {
                                    informations = errorInformations
                                }
                            }
                            else {
                                if let error = errors.first, let errorDescription = error["message"] as? String, let errorCode = error["code"] as? Int {
                                    
                                    innerError = CoreError(code: errorCode, description: errorDescription, innerError: nil, informations: nil)
                                }
                            }
                        } else {
                            if let error = dataObject as? [String: AnyObject] , let errorDescription = error["message"] as? String {
                                
                                innerError = CoreError(code: Int(RAND_MAX), description: errorDescription, innerError: nil, informations: nil)
                            }
                        }
                        
                        switch (httpUrlResponseErrorCode) {
                        case HTTPStatusCode.unAuthorized.rawValue:
                            completion({ () -> Data? in
                                throw UnAuthorisedError(innerError: innerError)
                            })
                        case HTTPStatusCode.forbidden.rawValue:
                            completion({ () -> Data? in
                                throw ForbiddenError(innerError: innerError)
                            })
                        case HTTPStatusCode.internal.rawValue:
                            completion({ () -> Data? in
                                throw InternalError(innerError: innerError)
                            })
                        case HTTPStatusCode.unProcessableEntity.rawValue:
                            completion({ () -> Data? in
                                throw UnProcessableEntityError(innerError: innerError, informations: informations)
                            })
                        case HTTPStatusCode.notFound.rawValue:
                            completion({ () -> Data? in
                                throw NotFoundError(innerError: innerError, informations: informations)
                            })
                        default:
                            completion({ () -> Data? in
                                throw CoreError(code: httpUrlResponseErrorCode, description: "Un Handled Error", innerError: innerError, informations: nil)
                            })
                        }
                    }
                })
                }.resume()
        }
    }
    
    fileprivate func createDataBody(withParameters params: Parameters?, media: [MediaData]?, boundary: String) -> Data {
        let lineBreak = "\r\n"
        var body = Data()
        
        if let parameters = params {
            for (key, value) in parameters {
                if (value is NSNull) == false {
                    body.append("--\(boundary + lineBreak)")
                    body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                    body.append("\( String(describing: value) + lineBreak)")
                }
            }
        }
        
        if let media = media {
            for photo in media {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(photo.key ?? "file")\"; filename=\"\(photo.filename ?? "ImageName.jpg")\"\(lineBreak)")
                body.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                body.append(photo.data)
                body.append(lineBreak)
            }
        }
        
        body.append("--\(boundary)--\(lineBreak)")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
}

extension Data {
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}

public typealias Parameters = [String: Any]

public enum MimeType:String { //--- add MimeType depends on your requirement
    case jpeg = "image/jpeg"
    case png = "image/png"
    case gif = "image/gif"
    case pdf = "application/pdf"
}

fileprivate struct MediaData {
    let key: String!
    let filename: String!
    let data: Data!
    let mimeType: String!
    
    init?(_ image: UIImage! = nil, data: Data? = nil, key: String, imageName: String, mimeType:MimeType) {
        self.key = key
        self.mimeType = mimeType.rawValue
        self.filename = imageName
        if data != nil {
            self.data = data
        } else {
            if let data = image.jpegData(compressionQuality: 0.5) {
                self.data = data
            } else {
                self.data = Data()
            }
        }
    }
}

extension CoreDataService: URLSessionDelegate {
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let isNeedToTrust: Bool = self.currentEnvironment == .production ? false : true
        if isNeedToTrust {
            completionHandler(Foundation.URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
        }
    }
}

